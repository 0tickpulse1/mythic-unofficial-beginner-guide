### [Background Information](Background-Information)
* [What is Mythic?](Background-Information#what-is-mythic)
* [What does Mythic consist of?](Background-Information#what-does-mythic-consist-of)
* [How do I install Mythic?](Background-Information#how-do-i-install-mythic)