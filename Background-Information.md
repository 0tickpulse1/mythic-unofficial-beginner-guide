# What is Mythic?

Mythic is a suite of plugins developed by MythicCraft, focused on providing an immersive RPG experience to your players. However, you do not even need to have an RPG server to make huge use of it.

Here is an example of a simple _mob_ made in MythicMobs:
```yml
WanderingAdventurer:
  Type: ZOMBIE
  Health: 20
  Damage: 2
  Display: 'Wandering Adventurer'
  Equipment:
  - IRON_SWORD HAND
  Skills:
  - heal{amount=5} @self ~onTimer:200
```
This is a mob called the Wandering Adventurer. It's base mob is a Zombie, it holds an Iron Sword in its hand, and heals itself every 10 seconds.

# What does Mythic consist of?

* **Mythic (+Mobs)**  
  The main Mythic plugin. It allows for complete control over mobs in your server. You can make mage zombies that shoot fireballs, friendly skeletons, buffed endermen, custom drops; your imagination is your limit!

* **ModelEngine**  
  ModelEngine is a plugin that allows you to make custom models to mobs, providing a modded mob experience unlike any other.

* **MythicCrucible**  
  MythicCrucible is a plugin that expands on Mythic's items. It adds custom furniture, backpacks, and many more features are coming.

* **MythicEnchants**  
  Add custom enchants to your server using the powerful Mythic Skill system.

* **MythicDungeons**  
  Create custom instanced dungeons in your server with unlimited freedom!

* **MythicScript** (Upcoming as of 22 July 2022)  
  Use Mythic's skill system to its full potential and expand beyond just mobs and items. You can fundamentally change the way your server works.

# How do I install Mythic?
## Requirements & recommendations
Before downloading Mythic, it is importnat that you recognize what is required.

Firstly, you **need** to be running a Spigot server, but it is **recommended** to run a fork of Spigot called Paper (or any of its forks).

You also need to be running your server on a recent Minecraft version.
### What is a "recent" Minecraft version?
A recent version usually means the newest version of Minecraft. However, it is also fine if you're one or two versions behind. As of writing this, the latest Minecraft version is `1.19.1`, so `1.18` and `1.17` are acceptable. However, you always need to be on the latest _patchversion_.

#### What are patchversions?
As of writing this, versions are written in this format:

`1.(major update number).(minor update number)`

The patchversion is the version with the highest `minor update number`. Patchversions include `1.18.2`, `1.17.1`, `1.16.4`, `1.15.2`, `1.14.4`, etc.
### Why Paper?
You might be wondering why we recommend you to use Paper instead of Spigot. Paper has a lot of enhanced or new features compared to Spigot, like an updated Timings, or a built-in anti-xray. Furthermore, it is much more performant and significantly faster than Spigot. Additionaly, many exploits are fixed, including dupe glitches.

#### Will plugins made for Spigot work for Paper?
Most of the time, most plugins that are developed for Spigot will work for Paper. However, this is not true the other way around. Many plugins made for Paper will not work on Spigot.

## Should I use developer builds or release builds?
Firstly, a build is a compiled version of the plugin's code. When code is "built", computer software reads the plugin's code, and translates it into binary, a language that computers can understand. When you're running a program, you're not actually running Java/C++/Python code that was used to originally write the program, you're running built-in Binary instructions that are "built" from the code.

A **developer build** (dev-build for short) is an experimental build that adds unreleased features or bug fixes. They have not been tested, and are generally not recommended to use in production servers.

A **release build** is a build that has been tested to be stable and safe to run on a production server.

Generally, for normal users, you should run the release build. If you run Developer builds, expect to update every few days to the latest Developer build.