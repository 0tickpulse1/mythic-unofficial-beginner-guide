# What is Mythic?

Mythic is a suite of plugins developed by MythicCraft, focused on providing an immersive RPG experience to your players. However, you do not even need to have an RPG server to make huge use of it.

* **Mythic (+Mobs)**  
  The main Mythic plugin. It allows for complete control over mobs in your server. You can make mage zombies that shoot fireballs, friendly skeletons, buffed endermen, custom drops; your imagination is your limit!

* **ModelEngine**  
  ModelEngine is a plugin that allows you to make custom models to mobs, providing a modded mob experience unlike any other.

* **MythicCrucible**  
  MythicCrucible is a plugin that expands on Mythic's items. It adds custom furniture, backpacks, and many more features are coming.

* **MythicEnchants**  
  Add custom enchants to your server using the powerful Mythic Skill system.

* **MythicDungeons**  
  Create custom instanced dungeons in your server with unlimited freedom!

# What will this guide consist of?
This is **not a wiki**. This will only teach you the basics of Mythic. If you already know the basics and want to learn more about the plugins, you should visit their official wiki instead. 

Instead, this wiki will assume the reader to have zero knowledge in server development. It will guide you along the way of grasping Mythic, nd potentially even other plugins.
