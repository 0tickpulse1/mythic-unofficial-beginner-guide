This guide is **not a wiki**. This will only teach you the basics of Mythic. If you already know the basics and want to learn more about the plugins, you should visit their official wiki instead. 

Instead, this wiki will assume the reader to have zero knowledge in server development. It will guide you along the way of grasping Mythic, nd potentially even other plugins.

If you're a total beginner, start with [Background Information](Background-Information). It will teach you what Mythic is, and what it's capable of.